import time
from cryptography.fernet import Fernet

#inicia contagem de tempo
start = time.time()

key = Fernet.generate_key()
f = Fernet(key)
token = f.encrypt(b"A really secret message. Not for prying eyes.")
# print(token)
f.decrypt(token)
end = time.time()
print("Runtime = " + str(end - start) + " Seconds")
# print(f.decrypt(token))
